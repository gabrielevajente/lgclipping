# Laguerre-Gauss Mode Clipping

Compute the analytical clipping matrix for Laguerre-Gauss modes, in the case of a circular aperture.

(2022) Gabriele Vajente, LIGO Laboratory, Caltech
vajente@caltech.edu

Companion code to the article 
    Vajente, "Analytical expressions for the clipping of
    Laguerre- and Hermite-Gauss modes by circular apertures"
    XXXX, YY (2022)
