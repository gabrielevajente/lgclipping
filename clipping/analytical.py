"""
Functions tompute Laguerre-Gauss clipping coefficients using analytical formula.

(2022) Gabriele Vajente, LIGO Laboratory, Caltech
vajente@caltech.edu

Companion code to the article 
    Vajente, "Analytical expressions for the clipping of
    Laguerre- and Hermite-Gauss modes by circular apertures"
    XXXX, YY (2022)
"""

import numpy
import sys
import time

# Arbitrary precision mathematics
import mpmath
mpmath.mp.dps = 30

from scipy.sparse import csr_matrix
from .laguerregauss import *

def clipping_matrix(rb, w, phig, N):
    """
    Compute the LG mode clipping matrix for an aperture with radius rb, and a beam radius w, Gouy phase phig.
    
    Parameters
    ----------
    rb: float
        aperture radius [m]
    w: float
        beam radius [m]
    phig: float
        Gouy phase at aperture position [rad]
    N: int
        maximum LG mode order
        
    Returns
    -------
    C: numpy.array
        matrix of coupling coefficients
    modes: list
        list of LG mode indexes
    """
    
    modes = LG_indexes_uptoN(N)
    Nmodes = len(modes)
    C = numpy.zeros((Nmodes, Nmodes))
    
    # precompute factorials
    nF = numpy.array([m[0] for m in modes]).max() + numpy.array([m[1] for m in modes]).max()
    F = numpy.zeros(nF+1, dtype=mpmath.mpf)
    for i in range(nF + 1):
        F[i] = mpmath.factorial(i)
        
    rhob = mpmath.mpf(numpy.sqrt(2) * rb / w)

    H = numpy.zeros((3*nF+3,3*nF+3), dtype=mpmath.mpf)

    start_time = time.time()
    for mi in range(Nmodes):
        for mj in range(mi,Nmodes):
            l,p = modes[mj]
            m,q = modes[mi]
            if m == l:
                c = mpmath.mpf(0)
                for j in range(0, min(p,q)+1):
                    if H[p+q+abs(l)+1, abs(l)+2*j+2] == 0:
                        H[p+q+abs(l)+1, abs(l)+2*j+2] = mpmath.hyp1f1(p+q+abs(l)+1, abs(l)+2*j+2, -rhob**2)
                    c += rhob**(2*abs(l)+4*j+2)* (numpy.sqrt(F[p]*F[q]*F[p+abs(l)]*F[q+abs(l)]) \
                                                  / (F[j]*F[p-j]*F[q-j]*F[j+abs(l)]*F[abs(l)+2*j+1]) ) \
                                                  * H[p+q+abs(l)+1, abs(l)+2*j+2]
                C[mi,mj] = (c * numpy.exp(2j*(p-q)*phig)).real
                C[mj,mi] = numpy.conj(C[mi,mj])

        elapsed = time.time() - start_time
        eta = elapsed / (mi+1) * (Nmodes - mi - 1)
        sys.stdout.write('\r%5d / %5d ELAPSED = %4d s, ETA = %4d s' % (mi+1, Nmodes, elapsed, eta))    
        
    return C, modes