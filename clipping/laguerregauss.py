"""
Collection of functions to compute Laguerre-Gauss modes and mode clipping coefficients.

(2022) Gabriele Vajente, LIGO Laboratory, Caltech
vajente@caltech.edu

Companion code to the article 
    Vajente, "Analytical expressions for the clipping of
    Laguerre- and Hermite-Gauss modes by circular apertures"
    XXXX, YY (2022)
"""

import numpy
import sys
from scipy.special import genlaguerre, factorial

def LG_indexes(N):
    """
    Return all the indexes pairs such that abs(l)+2*p = N
    """
    idx = []
    for p in range(N//2+1):
        l = N - 2*p
        if l > 0:
            idx.append( [l,p] )
            idx.append( [-l,p])
        elif l == 0:
            idx.append( [l, p])
    return idx

def LG_indexes_uptoN(N):
    """
    Return all the indexes pairs such that abs(l)+2*p <= N
    """

    idx = []
    for n in range(N+1):
        idx += LG_indexes(n)
    return idx


def LG_modes(w0, z, N, x, y, lb):
    """
    Return all LG modes up to order N, sampled at x,y.

    Parameters
    ----------
    w0: float
        beam waist size [m]
    z: float
        distance from waist [m]
    N: int
        maximu mode order (abs(l)+2*p <= N)
    x, y: numpy.array, numpy.array
        set of coordinates where the field is computed
    lb: float
        wavelength
    dxdy: bool
        if True, multiply by dx*dy so that integration is
        simply a sum

    Returns
    -------
    lg: numpy.array
        array of LG modes, the last index counts the modes
    modes: list of (int,int)
        l,p indexes for each mode
    """
    
    # list of all modes
    modes = LG_indexes_uptoN(N)
    Nmodes = len(modes)
       
    # compute w and inverse of R
    zr = numpy.pi*w0**2/lb
    k = 2*numpy.pi/lb
    w = w0 * numpy.sqrt(1 + (z/zr)**2)
    if z != 0:
        iR = 1/(z * ( 1 + (zr/z)**2 ))
    else:
        iR = 0
    
    gouy0 = numpy.arctan(z/zr) # Gouy phase 
    
    # cartesian to polar coordinates
    r = numpy.sqrt(x**2+y**2)
    t = numpy.arctan2(y,x)
    
    # common parts for all modes        
    lg = numpy.repeat(numpy.exp(-r**2/w**2 - 1j*k*r**2*iR/2).reshape(r.shape[0],r.shape[1],1), Nmodes, axis=-1) 
    # loop over all modes
    for i,m in enumerate(modes):
        sys.stdout.write('\r %d / %d' % (i+1, len(modes)))
        l,p = m
        gouy = (numpy.abs(l) + 2*p + 1) * gouy0      # mode Gouy's phase
        C = numpy.sqrt( 2*factorial(p) / numpy.pi / factorial(p + abs(l)) ) / w # mode normalization
        lg[:,:,i] = lg[:,:,i] * C * (r*numpy.sqrt(2)/w)**numpy.abs(l)  * \
                     genlaguerre(p, numpy.abs(l))(2*r**2/w**2) * numpy.exp(-1j*l*t + 1j*gouy)
        
    sys.stdout.write('\n')
    return lg, modes

def mode(coeff, coeff0, w0, z, x, lb, N):
    """
    Compute the shape of the beams with LG components specified in coeff and coeff0.
    
    Parameters
    ----------
    coeff, coeff0: numpy.array
        list of LG mode coefficients for two beams
    w0: float
        waist size
    z: float
        distance from waist
    x: numpy.array
        transverse coordinate array
    lb: float
        wavelength
    N: int
        maximum mode order
        
    Returns
    -------
    psi, psi0: numpy.array
        field amplitudes computed at x
        
    """
    # list of all modes
    modes = LG_indexes_uptoN(N)
    Nmodes = len(modes)
       
    # compute w and inverse of R
    zr = numpy.pi*w0**2/lb
    k = 2*numpy.pi/lb
    w = w0 * numpy.sqrt(1 + (z/zr)**2)
    if z != 0:
        iR = 1/(z * ( 1 + (zr/z)**2 ))
    else:
        iR = 0
    
    gouy0 = numpy.arctan(z/zr) # Gouy phase 
    
    r2 = x**2
    r = numpy.sqrt(r2)
       
    reconstructed  = numpy.zeros_like(x, dtype=numpy.clongdouble)
    reconstructed0 = numpy.zeros_like(x, dtype=numpy.clongdouble)

        
    # common parts for all modes        
    lg = numpy.exp(-r2/w**2 - 1j*k*r2*iR/2).astype(numpy.clongdouble)
    
    # loop over all modes
    for i,m in enumerate(modes):
        if coeff[i] !=0 or coeff0[i] != 0:
            sys.stdout.write('\r %d / %d' % (i+1, len(modes)))
            l,p = m
            gouy = (2*p + numpy.abs(l) + 1) * gouy0      # mode Gouy's phase
            C = sqrt( 2/pi/w**2 * (factorial(p) / factorial(p+abs(l)) ) )
            lgmn = lg * C * (numpy.sqrt(2)*r/w)**numpy.abs(l) * \
                       genlaguerre(p,numpy.abs(l))(2*r2/w**2) * numpy.exp(1j*gouy)
            reconstructed += coeff[i] * lgmn
            reconstructed0 += coeff0[i] * lgmn
        
    sys.stdout.write('\n')
    return reconstructed, reconstructed0

def radial_coupling(p, l, rb, w0, z, N, r, lb):
    """
    Compute the LG decomposition of a clipped mode.
    
    Parameters
    ----------
    p,l: int
        LG mode index of the input mode
    rb: float
        baffle radius
    w0: float
        waist size
    z: float
        distance from waist
    N: int
        maximum mode order (2p+abs(l))
    r: numpy.array
        vector of radial coordinates for the numerical inegration
    lb: float
        wavelength
        
    Returns
    -------
    coeff: numpy.array
        list of coefficients of the clipped mode
    modes: list of [int,int]
        corresponding mode index list

    
    """
    # list of all modes
    modes = LG_indexes_uptoN(N)
    Nmodes = len(modes)
    coeff = numpy.zeros(Nmodes, dtype=numpy.clongdouble)
       
    # compute w and inverse of R
    zr = numpy.pi*w0**2/lb
    k = 2*numpy.pi/lb
    w = w0 * numpy.sqrt(1 + (z/zr)**2)
    gouy0 = numpy.arctan(z/zr)
    
    r2 = r**2    
    mask = r2 <= rb**2
            
    # common parts for all modes        
    lg = numpy.exp(-2*r2/w**2)
    
    # loop over all modes
    for i,mod in enumerate(modes):
        sys.stdout.write('\r %d / %d' % (i+1, len(modes)))
        m,q = mod
        if m == l:
            gouy = (2*q-2*p) * gouy0      # mode Gouy's phase
            coeff[i] = 2 * numpy.sqrt(2)/w * numpy.sqrt(factorial(p)*factorial(q)/factorial(p+abs(l))/factorial(q+abs(l))) * numpy.exp(-2j*gouy) * \
                    numpy.trapz(mask * (r*numpy.sqrt(2)/w)**(1+2*abs(l)) * lg * genlaguerre(p,l)(2*r2/w**2) * genlaguerre(q,l)(2*r2/w**2), r)
        
        
    sys.stdout.write('\n')
    return coeff, modes